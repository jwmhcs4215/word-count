# Word Count

# DIFFERENCE BETWEEN MAP AND FLATMAP

Map and flatMap are similar, in the sense they take a line from the input RDD and apply a function on it. The way they differ is that the function in map returns only one element, while function in flatMap can return a list of elements (0 or more) as an iterator.
Also, the output of the flatMap is flattened. Although the function in flatMap returns a list of elements, the flatMap returns an RDD which has all the elements from the list in a flat way (not a list).

Difference:
map(func) Return a new distributed dataset formed by passing each element of the source through a function func.

flatMap(func) Similar to map, but each input item can be mapped to 0 or more output items (so func should return a Seq rather than a single item).

The transformation function: map: One element in -> one element out. flatMap: One element in -> 0 or more elements out (a collection).

Example1:-
sc.parallelize([3,4,5]).map(lambda x: range(1,x)).collect() 
Output:
1, 2], [1, 2, 3], [1, 2, 3, 4

sc.parallelize([3,4,5]).flatMap(lambda x: range(1,x)).collect()
Output: 
[1, 2, 1, 2, 3, 1, 2, 3, 4] 

Example 2:
sc.parallelize([3,4,5]).map(lambda x: [x,  x*x]).collect() 
Output:
[[3, 9], [4, 16], [5, 25]]

sc.parallelize([3,4,5]).flatMap(lambda x: [x, x*x]).collect() 
Output:
[3, 9, 4, 16, 5, 25]

