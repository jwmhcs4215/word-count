import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._

val sc = new SparkContext("local", "Word Count", new SparkConf())
val out = "output"
val input = sc.textFile("textfile.txt").map(line => line.toLowerCase)

val wc = input.flatMap(line => line.split("""[^\p{IsAlphabetic}]+""")).map(word => (word, 1)).reduceByKey((count1, count2) => count1 + count2)
wc.collect

wc.saveAsTextFile(out)
sc.stop()
System.exit(0)